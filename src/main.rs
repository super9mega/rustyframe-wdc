use std::cmp;

struct Weapon {
    name: String,
    physical: [PhysicalDamageTypes; 3],
    elemental: [ElementalDamageTypes; 2],
    mode: WeaponMode,
    projectile_count: i16,
    status_chance: f32,
    rate_of_fire: f32,
    magazine_capacity: i32,
    ammo_drain: f32,
    reload_time: f32,
    crit_chance: f32,
    base_crit_multiplier: f32,
    crit_multiplier: f32,
} impl Weapon {
    fn new (name: String, 
            physical_mods: Vec<PhysicalDamageTypes>, 
            elemental_mods: Vec<ElementalDamageTypes>, 
            mode: WeaponMode, 
            crit_chance: f32, 
            crit_multiplier: f32) -> Weapon {
        let mut physical = [PhysicalDamageTypes::Empty, PhysicalDamageTypes::Empty, PhysicalDamageTypes::Empty];
        let mut elemental = [ElementalDamageTypes::Empty, ElementalDamageTypes::Empty];
        let mut crit_chance_internal = crit_chance;
        let mut crit_multiplier_internal = crit_multiplier;
        let mut damage_multiplier = 0.0;
        for (i, damage) in physical_mods.iter().enumerate() {
            physical[i] = *damage;
        }
        for (i, damage) in elemental_mods.iter().enumerate() {
            elemental[i] = *damage;
        }
        while crit_chance_internal > 100.0 {
            damage_multiplier = crit_multiplier_internal;
            crit_multiplier_internal += crit_multiplier - 1.0;
            crit_chance_internal -= 100.0;

        }
        Weapon {
            name: name,
            physical: physical,
            elemental: elemental,
            mode: mode,
            // Todo: add importable values for weapons
            projectile_count: 1,
            status_chance: 22.0,
            rate_of_fire: 12.5,
            magazine_capacity: 100,
            ammo_drain: 0.5,
            reload_time: 2.6,
            crit_chance: crit_chance_internal,
            base_crit_multiplier: damage_multiplier,
            crit_multiplier: crit_multiplier_internal
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum PhysicalDamageTypes {
    Empty,
    Slash(i32),
    Impact(i32),
    Puncture(i32),
}

#[derive(Clone, Copy, Debug)]
enum ElementalDamageTypes {
    Empty,
    Cold(i32),
    Electricity(i32),
    Heat(i32),
    Toxin(i32),
    Blast(i32),
    Corrosive(i32),
    Gas(i32),
    Magnetic(i32),
    Radiation(i32),
    Viral(i32)
}

#[derive(Debug)]
enum WeaponMode {
    Burst,
    Charge,
    Continuous,
    FullAuto,
    FullAutoRU,
    FullAutoBR,
    SemiAuto,
    SniperSA,
    SniperCharge,
    BowSA,
    BowCharge,
    BowFA
}

fn main() {
    let amprex = Weapon::new(
        "amprex".to_string(),
        vec![], 
        vec![ElementalDamageTypes::Electricity(22)],
        WeaponMode::Continuous, 
        225.6, 
        8.8);
    println!("The {} is a {:?} weapon with types {:?} and {:?}", &amprex.name, amprex.mode , amprex.elemental, amprex.physical);
    let damage = calculate_dammage_per_shot(&amprex);
    println!("The damage per shot of the {} is {}", amprex.name, damage);
    println!("crit chance is {}", amprex.base_crit_multiplier);
}

fn calculate_dammage_per_shot(weapon: &Weapon) -> f32 {
    println!("Implement me");
    let mut damage = 0;
    for i in weapon.physical.iter() {
        damage += match *i {
            PhysicalDamageTypes::Empty => 0,
            PhysicalDamageTypes::Impact(i) => i,
            PhysicalDamageTypes::Puncture(i) => i,
            PhysicalDamageTypes::Slash(i) => i,
        };
    }
    for i in weapon.elemental.iter() {
        damage += match *i {
            ElementalDamageTypes::Empty => 0,
            ElementalDamageTypes::Blast(i) => i,
            ElementalDamageTypes::Cold(i) => i,
            ElementalDamageTypes::Corrosive(i) => i,
            ElementalDamageTypes::Electricity(i) => i,
            ElementalDamageTypes::Gas(i) => i,
            ElementalDamageTypes::Heat(i) => i,
            ElementalDamageTypes::Magnetic(i) => i,
            ElementalDamageTypes::Radiation(i) => i,
            ElementalDamageTypes::Toxin(i) => i,
            ElementalDamageTypes::Viral(i) => i,
        };
    }
    let critical_damage = damage as f32 * weapon.crit_multiplier;
    let final_damage = (((100.0 - weapon.crit_chance) * weapon.base_crit_multiplier * damage as f32) + (weapon.crit_chance * critical_damage)) / 100.0;
    return final_damage;
}
